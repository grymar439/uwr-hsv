# Analiza częstotliwości oddechów i parametru RMSSD z RR

Aplikacja desktpowa do analizy danych RR, wykorzystująca bibliotekę **HeartPy**. Projekt składa się z trzech skryptów:
- **hrv_app.py** - główny plik odpowiedzialny za GUI i komunikację z funkcjoinalnością. Może być modyfikowany
- **hrv_analysis.py** - plik wygenerowany za pomocą rogramu QTDesigner (wspierający framework PYQT5 do generowania widoków z wykorzystaniem GUI)
- **rr_sample.py** - plik zawierający wszystkie funkcjonalności programu

Wersja Pythona, na której zrealizowany był projekt to **3.9.10**. W celu uruchomienia programu w pierwszej kolejności należy pobrać zależności za pomocą:

    pip install -r requirements.txt

W celu wygenerowania nowego pliku .exe należy wykonać:

    pip install pyinstaller

    pyinstaller hrv_app.py

Po wygenerowaniu pliku .exe, zanjdować sie on będzie w dolderze /dist. W celu uruchomienia aplikacji .exe wraz z plikiem **hrv_analysis.ui** należy skopiować do jednego, wspólnego folderu. 

Metody wyznaczania poszczególnych parametrów bazują na bibliotece [HeartPy](https://python-heart-rate-analysis-toolkit.readthedocs.io/en/latest/)


W aplikacji znajdują się pola ograniczone do wpisywania liczb:
* Okno czasowe [s] - należy uzupełnic o wartość całkowitoliczbową. Dla tego czasu będzie wyliczany uśredniony wynik BR i RMSSD
* Częstotliwość próbkowania [s] - należy uzupełnic o wartość całkowitoliczbową. Wartość określająca co ile sekund zostanie obliczony kolejny wynik dla zadanego okna czasowego.

Przykładowo ustawiając wartość okna na 60 sekund i częstotliwości próbkowania na 10, co 10 sekund będzie obliczany średni wynik BR i RMSSD z ostatniej minuty.

<p>Program obsługuje pliki z rozszerzeniem csv, wuwzględniające podany nagłówek:</p>

<p>"Exported Session</p>
<p>Date (DD/MM/YYYY hh:mm:ss):15/01/2023 10:30:17</p>
<p>Parameter:RR</p>
<p>Time (DD/MM/YYYY hh:mm:ss.ms);    Epoch;   RR Interval[ms];   Heart Rate[BPM] (from RRI);   "</p>

W celu obliczania czasu uwzględnia się **Time (DD/MM/YYYY hh:mm:ss.ms)**, a dane do uśrednienia pozyskiwane są z **RR Interval [ms]**. 
W razie zmian w pliku docelowym należy zmienić poniższy kod:

    df = pd.read_csv(filename,
                     skiprows=3,
                     header=0,
                     usecols=['Time (DD/MM/YYYY hh:mm:ss.ms)', 'Epoch', 'RR Interval [ms]',
                              'Heart Rate[BPM] (from RRI)'])
    df = df.dropna()
    df['Time (DD/MM/YYYY hh:mm:ss.ms)'] = pd.to_datetime(df['Time (DD/MM/YYYY hh:mm:ss.ms)'],
                                                         format='%d/%m/%Y %H:%M:%S.%f')
    df['Timestamp'] = df['Time (DD/MM/YYYY hh:mm:ss.ms)'].apply(lambda x: datetime.timestamp(x))
    data = df['RR Interval [ms]'].to_numpy()
    time = df['Timestamp'].values

Gdzie **skiprows** to ilość linijek do usunięcia, **header** to numer wiersza określającego nagłówek z nazwami kolumn po usunięciu wierszy,
**usecols** to kolumny, które zaweira nagłówek. Poniżej w **data** i **time** zmodyfikować można nazwy kolumn, z których wyznaczany będzie czas i brane 
będą dane do analizy. Fragment pochodzi z pliku rr-sample.py.

W celu wygenerowania pliku *.exe istniejącej aplikacjiu należy uruchomić poniższą komendę w terminalu:

    pyinstaller --onefile hrv_app.py

Wygeneruje to plik "hrv_app.exe". Aby uruchomić aplikację, konieczne jest skopiowanie pliku hrv_analysis.ui do tego samego folderu. (Docelowa ścieżka, pod którą zostanie umieszczona aplikacja
nie ma znaczenia, ważne jest aby plik *.ui wskazany powyżej znalazł się w tym samym folderze co exe).



Biblioteka objęta jest licencją MIT:



Copyright (c) 2021 Paul van Gent

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
