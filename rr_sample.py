import pandas as pd
import numpy as np
from heartpy.analysis import calc_ts_measures, calc_breathing
from datetime import datetime

def nans(shape, dtype=float):
    """
    Function that creates array full of null values
    :param shape: shape of array
    :param dtype: type of values in array
    :return:  array
    """
    a = np.empty(shape, dtype)
    a.fill(np.nan)
    return a


def breathing_rate_with_moving_avg(filename, time_interval, sample_interval):
    """
    Main function, that calculater BR and HRV from given data
    To apply different file type modify:
    - skiprows - number of rows to be skipped in file (e.g. some additional information lie sensor output
    - header - number of row (after skipped) that contains column names, remove id file does not have column names
    - column names that will be in dataframe (suggested col names from file)
        :param filename: path to file
        :param time_interval: [seconds] window size
        :param sample_interval: [seconds] value gor moving average propagation
        :return: set od results packer in matrix containing BR and HRV params from partial measurements and whole
        dataset
        """
    res_br = []
    res_hrv = []
    df = pd.read_csv(filename,
                     skiprows=3,
                     header=0,
                     usecols=['Time (DD/MM/YYYY hh:mm:ss.ms)', 'Epoch', 'RR Interval [ms]',
                              'Heart Rate[BPM] (from RRI)'])
    df = df.dropna()
    df['Time (DD/MM/YYYY hh:mm:ss.ms)'] = pd.to_datetime(df['Time (DD/MM/YYYY hh:mm:ss.ms)'],
                                                         format='%d/%m/%Y %H:%M:%S.%f')
    df['Timestamp'] = df['Time (DD/MM/YYYY hh:mm:ss.ms)'].apply(lambda x: datetime.timestamp(x))
    data = df['RR Interval [ms]'].to_numpy()
    time = df['Timestamp'].values
    print(time)
    b = len(data)
    c = len(time)
    print(time[0])
    print(time[len(data) - 1])
    # calc  from whole dataset
    res_br_all = br_algo(data)
    res_hrv_all = hrv_algo(data)

    # while loop to calculate BR and HRV on specific time interval and sampling rate
    i = 0
    while time[0] + i + time_interval < time[len(data) - 1]:
        v1 = time[0] + i
        v2 = time[0] + i + time_interval
        df_start = df.iloc[(df['Timestamp'] - v1).abs().argsort()[:1]]
        df_end = df.iloc[(df['Timestamp'] - v2).abs().argsort()[:1]]
        data_calc = data[df_start.index[0]:df_end.index[0]]
        if len(data_calc) != 0:
            res_br.append(br_algo(data_calc))
            res_hrv.append(hrv_algo(data_calc))
        i += sample_interval
    # creating array of null values (it is needed because of concat results to one set and matrix could not contain
    # cols with different length
    if len(res_br) > 0:
        nan_arr = nans(len(res_br) - 1)
        results = [res_br, np.concatenate(([res_br_all], nan_arr)), res_hrv, np.concatenate(([res_hrv_all], nan_arr))]
    else:
        results = [0, 0, 0, 0]

    return results


def br_algo(data):
    """
        Function that calculates breathing rate from given data, uses calc_breathig from library heartpy.analysis
        :param data: array of numbers describing RR interval [ms]
        :return: Breathing rate for given dataset
        """
    rr_diff = np.diff(data)
    rr_sqdiff = np.power(rr_diff, 2)
    wd, m = calc_ts_measures(data, rr_diff, rr_sqdiff)
    m, wd = calc_breathing(data, measures=m, working_data=wd)
    return round(60 / (1 / m['breathingrate']), 5)


def hrv_algo(data):
    """
    Function that calculates hrv parameter from given data, uses calc_breathig from library heartpy.analysis
    :param data: array of numbers describing RR interval [ms]
    :return: rmssd for given dataset
    """
    rr_diff = np.diff(data)
    rr_sqdiff = np.power(rr_diff, 2)
    wd, m = calc_ts_measures(data, rr_diff, rr_sqdiff)
    m, wd = calc_breathing(data, measures=m, working_data=wd)
    return round(m['rmssd'], 5)


def save_results_to_file(filename, res_data, time_interval, sample_interval):
    """
    Saves results to csv file as filename_results.csv with given time interval and sample interval
    :param filename: Name of the file
    :param res_data: matrix of results as: partial br, br from whole set, hrv, hrv from whole set
    """
    name = filename[:-4] + "okno_" + str(time_interval) +"czest_probk_" + str(sample_interval) + "_results.csv"
    df = pd.DataFrame({'BR [/min]': res_data[0], 'BR (whole set) [\min]': res_data[1], 'RMSSD': res_data[2],
                       'RMSSD (whole set)': res_data[3]})
    df.to_csv(name, index=False)

