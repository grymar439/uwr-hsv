# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'hrv_analysis.ui'
#
# Created by: PyQt5 UI code generator 5.15.9
#
# File that consists of GUI operations, may be modified
import sys
import os

if getattr(sys, 'frozen', False):
    RELATIVE_PATH = os.path.dirname(sys.executable)
else:
    RELATIVE_PATH = os.path.dirname(__file__)

from PyQt5 import QtWidgets, uic
from PyQt5.QtWidgets import QMainWindow, QFileDialog, QMessageBox
import rr_sample as calc


class MainWIndow(QMainWindow):
    def __init__(self):
        super().__init__()
        self._ui_path = RELATIVE_PATH
        uic.loadUi(os.path.join(self._ui_path,'hrv_analysis.ui'), self)
        self.btnChooseFile.clicked.connect(self.choseFile)
        self.tfWindowSize.setInputMask("0000")
        self.tfSamplingRate.setInputMask("0000")
        self.btnCalculateParams.clicked.connect(self.calculateParams)
        self.lblBrResults.setText("BR: ")
        self.lblRmssdResults.setText("RMSSD: ")

    def choseFile(self):
        name = QFileDialog.getOpenFileName(self, 'Open File', "", "CSV Files (*.csv)")
        if name:
            self.lblCHosenFile.setText(str(name[0]))
            print(type(name))

    def calculateParams(self):
        try:
            res = calc.breathing_rate_with_moving_avg(str(self.lblCHosenFile.text()), int(self.tfWindowSize.text()),
                                                      int(self.tfSamplingRate.text()))
            self.lblBrResults.setText("BR: " + str(res[1][0]))
            self.lblRmssdResults.setText("RMSSD: " + str(res[3][0]))
            calc.save_results_to_file(str(self.lblCHosenFile.text()), res, int(self.tfWindowSize.text()),  int(self.tfSamplingRate.text()))
            msg1 = QMessageBox()
            msg1.setIcon(QMessageBox.Information)
            msg1.setText("Poprawnie zapisano do pliku")
            msg1.setWindowTitle("Zapis OK")
            msg1.exec_()
        except:
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Critical)
            msg.setText("Wystąpił błąd")
            msg.setInformativeText('Uzupełnij wszystkie dane lub sprawdź poprawność wgrywanego pliku CSV')
            msg.setWindowTitle("Error")
            msg.exec_()


# app execution
if __name__ == '__main__':
    import sys

    app = QtWidgets.QApplication(sys.argv)
    loginWindow = MainWIndow()
    loginWindow.show()
    sys.exit(app.exec_())

